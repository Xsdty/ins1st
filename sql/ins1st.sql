/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : ins1st

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 15/05/2019 13:48:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_biz_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_biz_log`;
CREATE TABLE `sys_biz_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志标题',
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '记录参数',
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '执行类',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '执行方法',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='业务日志';

-- ----------------------------
-- Records of sys_biz_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_biz_log` VALUES (39, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.controller.SysDictController', 'save', '2019-05-15 10:18:24');
INSERT INTO `sys_biz_log` VALUES (40, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.controller.SysDictController', 'save', '2019-05-15 10:18:48');
INSERT INTO `sys_biz_log` VALUES (41, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.controller.SysDictController', 'save', '2019-05-15 10:19:05');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `code` varchar(255) DEFAULT NULL COMMENT '值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict` VALUES (69, 1, 0, '性别', 'sex');
INSERT INTO `sys_dict` VALUES (70, 1, 69, '男', '1');
INSERT INTO `sys_dict` VALUES (71, 2, 69, '女', '2');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `p_id` int(11) DEFAULT NULL COMMENT '父级菜单',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单地址',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单权限',
  `is_menu` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是不是菜单',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标(只限一级菜单使用)',
  `sort` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单排序',
  `level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单层级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '#', '#', '0', '&#xe699', '1', '1');
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', '/sys/sysUser/index', 'sys:user:index,sys:user:add,sys:user:edit,sys:user:del', '1', '', '2', '2');
INSERT INTO `sys_menu` VALUES (3, 1, '菜单管理', '/sys/sysMenu/index', 'sys:menu:index', '1', NULL, '3', '2');
INSERT INTO `sys_menu` VALUES (4, 1, '角色管理', '/sys/sysRole/index', 'sys:role:index', '1', NULL, '4', '2');
INSERT INTO `sys_menu` VALUES (6, 1, '业务日志管理', '/sys/sysBizLog/index', 'sys:log:index,sys:log:del', '1', NULL, '5', '2');
INSERT INTO `sys_menu` VALUES (28, 0, '代码生成', '/gen/index', '#', '1', '&#xe696', '7', '1');
INSERT INTO `sys_menu` VALUES (29, 0, '数据源监控', '/druid/login.html', '#', '1', '&#xe70c', '8', '1');
INSERT INTO `sys_menu` VALUES (30, 0, '接口文档', '/swagger-ui.html', '#', '1', '&#xe66c', '9', '1');
INSERT INTO `sys_menu` VALUES (31, 1, '字典管理', '/sys/sysDict/index', 'sys:dict:index,sys:dict:add,sys:dict:edit,sys:dict:del', '1', NULL, '10', '3');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` varchar(255) DEFAULT NULL COMMENT '角色状态',
  `keyword` varchar(255) DEFAULT NULL COMMENT '角色标识',
  `sort` varchar(255) DEFAULT NULL COMMENT '角色排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '超级管理员', '1', 'admin', '1');
INSERT INTO `sys_role` VALUES (7, '测试管理员', '1', 'test', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='角色和菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (55, 1, 1);
INSERT INTO `sys_role_menu` VALUES (56, 1, 2);
INSERT INTO `sys_role_menu` VALUES (57, 1, 3);
INSERT INTO `sys_role_menu` VALUES (58, 1, 4);
INSERT INTO `sys_role_menu` VALUES (59, 1, 6);
INSERT INTO `sys_role_menu` VALUES (60, 1, 31);
INSERT INTO `sys_role_menu` VALUES (61, 1, 28);
INSERT INTO `sys_role_menu` VALUES (62, 1, 29);
INSERT INTO `sys_role_menu` VALUES (63, 1, 30);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名称',
  `user_nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `user_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户密码',
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户邮箱',
  `user_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户电话',
  `user_sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户性别',
  `role_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户角色',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户状态（1正常2冻结）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (4, 'admin', 'INS1ST贼牛逼', 'b4674fb71b5b62030be59814f75674c7814a89a424739272', '499719083@qq.com', '130000000', '1', '1,7', '1');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='用户和角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (58, 4, 1);
INSERT INTO `sys_user_role` VALUES (59, 4, 7);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
