package com.ins1st.model;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;

import java.util.List;

/**
 * 代码生成实体类
 *
 * @author sun
 */
public class GenEntity {

    private String author;
    private String tableName;
    private String moduleName;
    private String bizName;
    private boolean genController;
    private boolean genService;
    private boolean genServiceImpl;
    private boolean genEntity;
    private boolean genDao;
    private boolean genXml;
    private boolean genIndexHtml;
    private boolean genAddHtml;
    private boolean genEditHtml;
    private boolean genIndexJs;
    private boolean genAddJs;
    private boolean genEditJs;
    private String url;
    private String username;
    private String password;
    private List<TableInfo> infoList;

    public List<TableInfo> getInfoList() {
        return infoList;
    }

    public void setInfoList(List<TableInfo> infoList) {
        this.infoList = infoList;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getBizName() {
        return bizName;
    }

    public void setBizName(String bizName) {
        this.bizName = bizName;
    }

    public boolean isGenController() {
        return genController;
    }

    public void setGenController(boolean genController) {
        this.genController = genController;
    }

    public boolean isGenService() {
        return genService;
    }

    public void setGenService(boolean genService) {
        this.genService = genService;
    }

    public boolean isGenServiceImpl() {
        return genServiceImpl;
    }

    public void setGenServiceImpl(boolean genServiceImpl) {
        this.genServiceImpl = genServiceImpl;
    }

    public boolean isGenEntity() {
        return genEntity;
    }

    public void setGenEntity(boolean genEntity) {
        this.genEntity = genEntity;
    }

    public boolean isGenDao() {
        return genDao;
    }

    public void setGenDao(boolean genDao) {
        this.genDao = genDao;
    }

    public boolean isGenXml() {
        return genXml;
    }

    public void setGenXml(boolean genXml) {
        this.genXml = genXml;
    }

    public boolean isGenIndexHtml() {
        return genIndexHtml;
    }

    public void setGenIndexHtml(boolean genIndexHtml) {
        this.genIndexHtml = genIndexHtml;
    }

    public boolean isGenAddHtml() {
        return genAddHtml;
    }

    public void setGenAddHtml(boolean genAddHtml) {
        this.genAddHtml = genAddHtml;
    }

    public boolean isGenEditHtml() {
        return genEditHtml;
    }

    public void setGenEditHtml(boolean genEditHtml) {
        this.genEditHtml = genEditHtml;
    }

    public boolean isGenIndexJs() {
        return genIndexJs;
    }

    public void setGenIndexJs(boolean genIndexJs) {
        this.genIndexJs = genIndexJs;
    }

    public boolean isGenAddJs() {
        return genAddJs;
    }

    public void setGenAddJs(boolean genAddJs) {
        this.genAddJs = genAddJs;
    }

    public boolean isGenEditJs() {
        return genEditJs;
    }

    public void setGenEditJs(boolean genEditJs) {
        this.genEditJs = genEditJs;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
